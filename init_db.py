#!/usr/bin/python2.7
# -*- coding: utf8 -*-

__author__ = 'michal.lazo'

import time
import sqlobject
from sqlobject import *
import MySQLdb.converters

from sqlobject.col import StringCol

def _mysql_timestamp_converter(raw):
         """Convert a MySQL TIMESTAMP to a floating point number representing
         the seconds since the Un*x Epoch. It uses custom code the input seems
         to be the new (MySQL 4.1+) timestamp format, otherwise code from the
         MySQLdb module is used."""
         if raw[4] == '-':
             return time.mktime(time.strptime(raw, '%Y-%m-%d %H:%M:%S'))
         else:
             return MySQLdb.converters.mysql_timestamp_converter(raw)

conversions = MySQLdb.converters.conversions.copy()
conversions[MySQLdb.constants.FIELD_TYPE.TIMESTAMP] = _mysql_timestamp_converter

MySQLConnection = sqlobject.mysql.builder()
connection = MySQLConnection(user='streamer', password='streamer', db='streamer', conv=conversions, use_unicode = True, charset = "utf8", debug='true')

sqlhub.processConnection = connection

class Channel(SQLObject):
    vdrId = StringCol()
    name = StringCol()
    vdrIdIndex = DatabaseIndex('vdrId')
    nameIndex = DatabaseIndex('name')

class Event(SQLObject):
    vdrId = IntCol()
    startTime = BigIntCol()
    endTime = BigIntCol()
    duration = IntCol()
    title = StringCol()
    shortText = StringCol()
    description = StringCol()
    channel = ForeignKey('Channel')
    EventChannelIdExistsIndex = DatabaseIndex('channel')
    startTimeIndex = DatabaseIndex('startTime')
    vdrIdIndex = DatabaseIndex('vdrId','startTime')

class FileSegment(SQLObject):
    fileId = BigIntCol()
    startTime = BigIntCol()
    duration = IntCol()
    channel = ForeignKey('Channel')
    firstLastIndex = DatabaseIndex('fileId', 'channel')
    fileSegmentChannelIdExistIndex = DatabaseIndex('channel')
    startTimeIndex = DatabaseIndex('startTime', 'channel')


print("grand user to access db")


try:
    Channel.createTable()
except Exception as e:
    print("Channel createTable problem ")
    print str(e)
    pass

print

try:
    Event.createTable()
except Exception as e:
    print("Event createTable problem")
    print str(e)
    pass

print

try:
    FileSegment.createTable()
except Exception as e:
    print("FileSegment createTable problem")
    print str(e)
    pass
